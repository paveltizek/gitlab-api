<?php
/**
 * User: paveltizek
 * Date: 6.7.17
 * Time: 6:53
 */

namespace PavelTizek\GitLab\Model;


use PavelTizek\GitLab\Client;

class AbstractModel
{
    /** @var  Client */
    protected $client;

    public function api($apiName){
        return $this->client->api($apiName);
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }



}