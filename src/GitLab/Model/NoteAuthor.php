<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 14:49
 */

namespace PavelTizek\GitLab\Model;


class NoteAuthor
{
    private $id;
    private $name;
    private $username;
    private $state;
    private $avatarUrl;
    private $webUrl;

    /**
     * NoteAuthor constructor.
     * @param $id
     * @param $name
     * @param $username
     * @param $state
     * @param $avatarUrl
     * @param $webUrl
     */
    public function __construct($id, $name, $username, $state, $avatarUrl, $webUrl)
    {
        $this->id = $id;
        $this->name = $name;
        $this->username = $username;
        $this->state = $state;
        $this->avatarUrl = $avatarUrl;
        $this->webUrl = $webUrl;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }

    /**
     * @return mixed
     */
    public function getWebUrl()
    {
        return $this->webUrl;
    }



}