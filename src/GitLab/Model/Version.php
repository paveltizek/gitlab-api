<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 15.7.17
 * Time: 13:57
 */

namespace PavelTizek\GitLab\Model;


class Version extends AbstractModel
{
    /** @var  string */
    private $version;

    /** @var  string */
    private $revision;

    /**
     * Version constructor.
     * @param string $version
     * @param string $revision
     */
    public function __construct($version, $revision)
    {
        $this->version = $version;
        $this->revision = $revision;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getRevision()
    {
        return $this->revision;
    }




}