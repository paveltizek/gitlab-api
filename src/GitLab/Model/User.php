<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 12:15
 */

namespace PavelTizek\GitLab\Model;


class User extends AbstractModel
{
    private $name;
    private $username;
    private $id;
    private $state;
    private $avatarUrl;
    private $webUrl;

    /**
     * Author constructor.
     * @param $name
     * @param $username
     * @param $id
     * @param $state
     * @param $avatarUrl
     * @param $webUrl
     */
    public function __construct($name, $username, $id, $state, $avatarUrl, $webUrl)
    {
        $this->name = $name;
        $this->username = $username;
        $this->id = $id;
        $this->state = $state;
        $this->avatarUrl = $avatarUrl;
        $this->webUrl = $webUrl;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }

    /**
     * @return mixed
     */
    public function getWebUrl()
    {
        return $this->webUrl;
    }




}