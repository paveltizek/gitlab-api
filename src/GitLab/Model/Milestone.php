<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 12:19
 */

namespace PavelTizek\GitLab\Model;


class Milestone extends AbstractModel
{
    private $id;
    private $iid;
    private $projectId;
    private $title;
    private $description;
    private $state;
    private $createdAt;
    private $updatedAt;
    private $dueDate;
    private $startDate;

    /**
     * Milestone constructor.
     * @param $id
     * @param $iid
     * @param $projectId
     * @param $title
     * @param $description
     * @param $state
     * @param $createdAt
     * @param $updatedAt
     * @param $dueDate
     * @param $startDate
     */
    public function __construct($id, $iid, $projectId, $title, $description, $state, $createdAt, $updatedAt, $dueDate, $startDate)
    {
        $this->id = $id;
        $this->iid = $iid;
        $this->projectId = $projectId;
        $this->title = $title;
        $this->description = $description;
        $this->state = $state;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->dueDate = $dueDate;
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIid()
    {
        return $this->iid;
    }

    /**
     * @return mixed
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }



}