<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 16:19
 */

namespace PavelTizek\GitLab\Model;


class Tag extends AbstractModel
{

    /** @var  Commit */
    private $commit;

    /** @var  Release */
    private $release;

    private $name;
    private $message;

    /**
     * Tag constructor.
     * @param Commit $commit
     * @param Release $release
     * @param $name
     * @param $message
     */
    public function __construct(Commit $commit, ?Release $release, $name, $message)
    {
        $this->commit = $commit;
        $this->release = $release;
        $this->name = $name;
        $this->message = $message;
    }

    /**
     * @return Commit
     */
    public function getCommit()
    {
        return $this->commit;
    }

    /**
     * @return Release
     */
    public function getRelease()
    {
        return $this->release;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    

}