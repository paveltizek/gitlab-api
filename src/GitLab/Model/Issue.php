<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 12:10
 */

namespace PavelTizek\GitLab\Model;


class Issue extends AbstractModel
{

    private $id;
    private $iid;
    private $projectId;
    private $title;
    private $description;
    private $state;
    private $createdAt;
    private $updatedAt;
    private $labels;

    /** @var  Milestone */
    private $milestone;

    /** @var  Author */
    private $author;
    private $userNotesCount;
    private $upVotes;
    private $downVotes;
    private $dueDate;
    private $confidental;
    private $weight;
    private $webUrl;
    private $subscribed;

    private $comments;

    /**
     * Issue constructor.
     * @param $id
     * @param $iid
     * @param $projectId
     * @param $title
     * @param $description
     * @param $state
     * @param $createdAt
     * @param $updatedAt
     * @param $labels
     * @param Milestone $milestone
     * @param Author $author
     * @param $userNotesCount
     * @param $upVotes
     * @param $downVotes
     * @param $dueDate
     * @param $confidental
     * @param $weight
     * @param $webUrl
     * @param $subscribed
     * @param $comments
     */
    public function __construct($id, $iid, $projectId, $title, $description, $state, $createdAt, $updatedAt, $labels, Milestone $milestone = null, Author $author = null, $userNotesCount, $upVotes, $downVotes, $dueDate, $confidental, $weight, $webUrl, $subscribed = null, $comments = [])
    {
        $this->id = $id;
        $this->iid = $iid;
        $this->projectId = $projectId;
        $this->title = $title;
        $this->description = $description;
        $this->state = $state;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->labels = $labels;
        $this->milestone = $milestone;
        $this->author = $author;
        $this->userNotesCount = $userNotesCount;
        $this->upVotes = $upVotes;
        $this->downVotes = $downVotes;
        $this->dueDate = $dueDate;
        $this->confidental = $confidental;
        $this->weight = $weight;
        $this->webUrl = $webUrl;
        $this->subscribed = $subscribed;
        $this->comments = $comments;
    }

    /**
     * @return array
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIid()
    {
        return $this->iid;
    }

    /**
     * @return mixed
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * @return Milestone
     */
    public function getMilestone()
    {
        return $this->milestone;
    }

    /**
     * @return Author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return mixed
     */
    public function getUserNotesCount()
    {
        return $this->userNotesCount;
    }

    /**
     * @return mixed
     */
    public function getUpVotes()
    {
        return $this->upVotes;
    }

    /**
     * @return mixed
     */
    public function getDownVotes()
    {
        return $this->downVotes;
    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @return mixed
     */
    public function getConfidental()
    {
        return $this->confidental;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return mixed
     */
    public function getWebUrl()
    {
        return $this->webUrl;
    }

    /**
     * @return null
     */
    public function getSubscribed()
    {
        return $this->subscribed;
    }

    public function isClosed(){
        return $this->state == 'opened';
    }








}