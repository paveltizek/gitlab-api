<?php
/**
 * User: paveltizek
 * Date: 2.8.17
 * Time: 7:38
 */

namespace PavelTizek\GitLab\Model;


class BroadcastMessage extends AbstractModel
{
    private $message;
    private $startsAt;
    private $endsAt;
    private $color;
    private $font;
    private $id;
    private $active;

    /**
     * BroadcastMessage constructor.
     * @param $message
     * @param $startsAt
     * @param $endsAt
     * @param $color
     * @param $font
     * @param $id
     * @param $active
     */
    public function __construct($message, $startsAt, $endsAt, $color, $font, $id, $active)
    {
        $this->message = $message;
        $this->startsAt = $startsAt;
        $this->endsAt = $endsAt;
        $this->color = $color;
        $this->font = $font;
        $this->id = $id;
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @return mixed
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return mixed
     */
    public function getFont()
    {
        return $this->font;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }




}