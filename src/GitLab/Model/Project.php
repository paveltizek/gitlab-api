<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 12:10
 */

namespace PavelTizek\GitLab\Model;


class Project extends AbstractModel
{

    private $id;
    private $name;
    private $nameWithNamespace;
    private $path;
    private $pathWithNamespace;

    /**
     * Project constructor.
     * @param $id
     * @param $name
     * @param $nameWithNamespace
     * @param $path
     * @param $pathWithNamespace
     */
    public function __construct($id, $name, $nameWithNamespace, $path, $pathWithNamespace)
    {
        $this->id = $id;
        $this->name = $name;
        $this->nameWithNamespace = $nameWithNamespace;
        $this->path = $path;
        $this->pathWithNamespace = $pathWithNamespace;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getNameWithNamespace()
    {
        return $this->nameWithNamespace;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getPathWithNamespace()
    {
        return $this->pathWithNamespace;
    }





}