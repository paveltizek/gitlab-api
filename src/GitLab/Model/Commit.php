<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 16:18
 */

namespace PavelTizek\GitLab\Model;


class Commit extends AbstractModel
{
    private $authorName;
    private $authorEmail;
    private $authoredDate;
    private $committedDate;
    private $committerName;
    private $committerEmail;
    private $id;
    private $message;
    /** @var  array */
    private $parentIds;

    /**
     * Commit constructor.
     * @param $authorName
     * @param $authorEmail
     * @param $authoredDate
     * @param $committedDate
     * @param $committerName
     * @param $committerEmail
     * @param $id
     * @param $message
     * @param array $parentIds
     */
    public function __construct($authorName, $authorEmail, $authoredDate, $committedDate, $committerName, $committerEmail, $id, $message, array $parentIds = [])
    {
        $this->authorName = $authorName;
        $this->authorEmail = $authorEmail;
        $this->authoredDate = $authoredDate;
        $this->committedDate = $committedDate;
        $this->committerName = $committerName;
        $this->committerEmail = $committerEmail;
        $this->id = $id;
        $this->message = $message;
        $this->parentIds = $parentIds;
    }

    /**
     * @return mixed
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @return mixed
     */
    public function getAuthorEmail()
    {
        return $this->authorEmail;
    }

    /**
     * @return mixed
     */
    public function getAuthoredDate()
    {
        return $this->authoredDate;
    }

    /**
     * @return mixed
     */
    public function getCommittedDate()
    {
        return $this->committedDate;
    }

    /**
     * @return mixed
     */
    public function getCommitterName()
    {
        return $this->committerName;
    }

    /**
     * @return mixed
     */
    public function getCommitterEmail()
    {
        return $this->committerEmail;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getParentIds()
    {
        return $this->parentIds;
    }



}