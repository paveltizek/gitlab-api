<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 16:19
 */

namespace PavelTizek\GitLab\Model;


class Release extends AbstractModel
{
    private $tagName;
    private $description;

    /**
     * Release constructor.
     * @param $tagName
     * @param $description
     */
    public function __construct($tagName, $description)
    {
        $this->tagName = $tagName;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTagName()
    {
        return $this->tagName;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }



}