<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 14:31
 */

namespace PavelTizek\GitLab\Model;


use Nette\Utils\DateTime;

class IssueComment extends AbstractModel
{
    private $id;
    private $body;
    private $attachment;

    /** @var  NoteAuthor */
    private $author;
    /** @var DateTime */
    private $createdAt;
    /** @var  DateTime */
    private $updatedAt;

    private $system;
    private $noteableId;
    private $noteableType;

    /**
     * IssueComment constructor.
     * @param $id
     * @param $body
     * @param $attachment
     * @param NoteAuthor $author
     * @param $createdAt
     * @param $updatedAt
     * @param $system
     * @param $noteableId
     * @param $noteableType
     */
    public function __construct($id, $body, $attachment, NoteAuthor $author, $createdAt, $updatedAt, $system, $noteableId, $noteableType)
    {
        $this->id = $id;
        $this->body = $body;
        $this->attachment = $attachment;
        $this->author = $author;
        $this->createdAt = DateTime::from($createdAt);
        $this->updatedAt = DateTime::from($updatedAt);
        $this->system = $system;
        $this->noteableId = $noteableId;
        $this->noteableType = $noteableType;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @return NoteAuthor
     */
    public function getAuthor()
    {
        return $this->author;
    }


    /**
     * @return DateTime|static
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * @return DateTime|static
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * @return mixed
     */
    public function getNoteableId()
    {
        return $this->noteableId;
    }

    /**
     * @return mixed
     */
    public function getNoteableType()
    {
        return $this->noteableType;
    }



}