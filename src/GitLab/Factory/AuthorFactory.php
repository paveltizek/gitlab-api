<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 15:28
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\Author;

class AuthorFactory extends AbstractFactory
{
    public function create(array $author){
        $newAuthor = new Author(
            $author['name'],
            $author['username'],
            $author['id'],
            $author['state'],
            $author['avatar_url'],
            $author['web_url']
        );

        $newAuthor->setClient($this->client);
        return $newAuthor;
    }

}