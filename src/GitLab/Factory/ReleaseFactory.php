<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 16:31
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\Release;

class ReleaseFactory extends AbstractFactory
{
    public function create($release){
        return $release ? new Release(
            $release['tag_name'],
            $release['description']
        ) : null;
    }

}