<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 15:28
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\Author;
use PavelTizek\GitLab\Model\User;

class UserFactory extends AbstractFactory
{
    public function create(array $user){
        $newUser = new User(
            $user['name'],
            $user['username'],
            $user['id'],
            $user['state'],
            $user['avatar_url'],
            $user['web_url']
        );

        return $newUser;
    }

}