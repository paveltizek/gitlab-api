<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 15:30
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\Milestone;

class MilestoneFactory extends AbstractFactory
{
    public function create(array $milestone){
        return new Milestone(
           $milestone['id'],
           $milestone['iid'],
           $milestone['project_id'],
           $milestone['title'],
           $milestone['description'],
           $milestone['state'],
           $milestone['created_at'],
           $milestone['updated_at'],
           $milestone['due_date'],
           $milestone['start_date']

        );
    }

}