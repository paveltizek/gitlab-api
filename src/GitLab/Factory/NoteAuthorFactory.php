<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 15:35
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\NoteAuthor;

class NoteAuthorFactory extends AbstractFactory
{
    public function create(array $author){
        return new NoteAuthor(
            $author['id'],
            $author['name'],
            $author['username'],
            $author['state'],
            $author['avatar_url'],
            $author['web_url']
        );
    }

}