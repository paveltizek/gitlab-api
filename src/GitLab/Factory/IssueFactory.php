<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 15:25
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\Issue;

class IssueFactory extends AbstractFactory
{
    public function create($jsonIssue, $milestone = null, $author = null, $comments = [])
    {

        return new Issue(
            $jsonIssue['id'],
            $jsonIssue['iid'],
            $jsonIssue['project_id'],
            $jsonIssue['title'],
            $jsonIssue['description'],
            $jsonIssue['state'],
            $jsonIssue['created_at'],
            $jsonIssue['updated_at'],
            $jsonIssue['labels'],
            $milestone,
            $author,
            $jsonIssue['user_notes_count'],
            $jsonIssue['upvotes'],
            $jsonIssue['downvotes'],
            $jsonIssue['due_date'],
            $jsonIssue['confidential'],
            isset($jsonIssue['weight']) ? $jsonIssue['weight'] : null,
            $jsonIssue['web_url'],
            isset($jsonIssue['subscribed']) ? $jsonIssue['subscribed'] : null,
            $comments
        );
    }

}