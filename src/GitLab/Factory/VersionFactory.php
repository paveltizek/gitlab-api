<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 15.7.17
 * Time: 13:57
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\Version;

class VersionFactory extends AbstractFactory
{

    public function create($versionArray){
        return new Version($versionArray['version'], $versionArray['revision']);
    }

}