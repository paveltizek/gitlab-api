<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 15:25
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Client;

class AbstractFactory
{
    /** @var  Client */
    protected $client;

    /**
     * AbstractFactory constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }


}