<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 15:27
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\IssueComment;

class IssueCommentFactory extends AbstractFactory
{
    public function create($jsonComment, $issueCommentAuthor){
        return new IssueComment(
            $jsonComment['id'],
            $jsonComment['body'],
            $jsonComment['attachment'],
            $issueCommentAuthor,
            $jsonComment['created_at'],
            $jsonComment['updated_at'],
            $jsonComment['system'],
            $jsonComment['noteable_id'],
            $jsonComment['noteable_type']
        );
    }

}