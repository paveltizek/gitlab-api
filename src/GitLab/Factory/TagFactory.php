<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 16:22
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\Commit;
use PavelTizek\GitLab\Model\Release;
use PavelTizek\GitLab\Model\Tag;

class TagFactory extends AbstractFactory
{
    public function create($tag, Commit $commit, ?Release $release){
        return new Tag($commit, $release, $tag['name'], $tag['message']);
    }

}