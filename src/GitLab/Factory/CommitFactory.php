<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 16:25
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\Commit;

class CommitFactory extends AbstractFactory
{
    public function create($commit){
        return new Commit(
            $commit['author_name'],
            $commit['author_email'],
            $commit['authored_date'],
            $commit['committed_date'],
            $commit['committer_name'],
            $commit['committer_email'],
            $commit['id'],
            $commit['message']
        );
    }

}