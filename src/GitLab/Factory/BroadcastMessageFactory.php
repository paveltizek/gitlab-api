<?php
/**
 * User: paveltizek
 * Date: 2.8.17
 * Time: 7:38
 */

namespace PavelTizek\GitLab\Factory;


use PavelTizek\GitLab\Model\BroadcastMessage;

class BroadcastMessageFactory extends AbstractFactory
{

    public function create($jsonBroadcastMessage){
        return new BroadcastMessage(
            $jsonBroadcastMessage['message'],
            $jsonBroadcastMessage['starts_at'],
            $jsonBroadcastMessage['ends_at'],
            $jsonBroadcastMessage['color'],
            $jsonBroadcastMessage['font'],
            $jsonBroadcastMessage['id'],
            $jsonBroadcastMessage['active']
        );
    }

}