<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 16:19
 */

namespace PavelTizek\GitLab\Api;


use GuzzleHttp\Exception\ClientException;
use Nette\Utils\Json;
use PavelTizek\GitLab\Exception\TagException;

class TagApi extends AbstractApi
{
    /**
     * @param $projectId
     * @return array
     */
    public function getAll($projectId){
        $jsonTags = Json::decode($this->get('/projects/' . $projectId . '/repository/tags'), Json::FORCE_ARRAY);

        $tags = [];
        foreach ($jsonTags as $jsonTag) {
            $tags[] = $this->tagFactory->create($jsonTag, $this->commitFactory->create($jsonTag['commit']), $this->releaseFactory->create($jsonTag['release']));
        }

        return $tags;
    }

    /**
     * @param $projectId
     * @param $tagName
     * @param $description
     * @return \PavelTizek\GitLab\Model\Release
     */
    public function createRelease($projectId, $tagName, $description){
        $args = [
            'description' => $description
        ];

        $jsonRelease = Json::decode($this->post('/projects/' . $projectId . '/repository/tags/' . $tagName . '/release', $args), Json::FORCE_ARRAY);

        $release = $this->releaseFactory->create($jsonRelease);

        return $release;
    }

    /**
     * @param $projectId
     * @param $tagName
     * @throws TagException
     */
    public function deleteTag($projectId, $tagName){

        try {
            $this->delete('/projects/' . $projectId . '/repository/tags/' . $tagName);
        } catch (ClientException $e) {
            throw new TagException('Tag not found');
        }
    }

}