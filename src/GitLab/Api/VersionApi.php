<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 15.7.17
 * Time: 13:56
 */

namespace PavelTizek\GitLab\Api;


use Nette\Utils\Json;

class VersionApi extends AbstractApi
{
    public function getVersion(){
        $versionArray = Json::decode($this->get('/version'), Json::FORCE_ARRAY);
        return $this->versionFactory->create($versionArray);
    }

}