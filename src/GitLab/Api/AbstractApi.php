<?php
/**
 * User: paveltizek
 * Date: 6.7.17
 * Time: 6:53
 */

namespace PavelTizek\GitLab\Api;


use PavelTizek\GitLab\Client;
use PavelTizek\GitLab\Factory\AuthorFactory;
use PavelTizek\GitLab\Factory\BroadcastMessageFactory;
use PavelTizek\GitLab\Factory\CommitFactory;
use PavelTizek\GitLab\Factory\IssueCommentAuthorFactory;
use PavelTizek\GitLab\Factory\IssueCommentFactory;
use PavelTizek\GitLab\Factory\IssueFactory;
use PavelTizek\GitLab\Factory\MilestoneFactory;
use PavelTizek\GitLab\Factory\NoteAuthorFactory;
use PavelTizek\GitLab\Factory\ReleaseFactory;
use PavelTizek\GitLab\Factory\TagFactory;
use PavelTizek\GitLab\Factory\UserFactory;
use PavelTizek\GitLab\Factory\VersionFactory;

class AbstractApi
{
    /** @var  Client */
    protected $client;

    protected $options;

    /** @var  AuthorFactory */
    protected $authorFactory;

    /** @var  CommitFactory */
    protected $commitFactory;

    /** @var  IssueCommentFactory */
    protected $issueCommentFactory;

    /** @var  IssueCommentAuthorFactory */
    protected $issueCommentAuthorFactory;

    /** @var  IssueFactory */
    protected $issueFactory;

    /** @var  MilestoneFactory */
    protected $milestoneFactory;

    /** @var  NoteAuthorFactory */
    protected $noteAuthorFactory;

    /** @var  ReleaseFactory */
    protected $releaseFactory;

    /** @var  TagFactory */
    protected $tagFactory;

    /** @var  VersionFactory */
    protected $versionFactory;

    /** @var  BroadcastMessageFactory */
    protected $broadcastMessageFactory;

    /** @var  UserFactory */
    protected $userFactory;

    /**
     * AbstractApi constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->options = $client->getOptions();

        $this->commitFactory = new CommitFactory($client);
        $this->issueCommentFactory = new IssueCommentFactory($client);
        $this->issueCommentAuthorFactory = new IssueCommentAuthorFactory($client);
        $this->issueFactory = new IssueFactory($client);
        $this->milestoneFactory = new MilestoneFactory($client);
        $this->noteAuthorFactory = new NoteAuthorFactory($client);
        $this->releaseFactory = new ReleaseFactory($client);
        $this->tagFactory = new TagFactory($client);
        $this->authorFactory = new AuthorFactory($client);
        $this->versionFactory = new VersionFactory($client);
        $this->broadcastMessageFactory = new BroadcastMessageFactory($client);
    }

    /**
     * @param $path
     * @param array $params
     * @return string
     */
    protected function get($path, array $params = []){
        $this->options['query'] = $params;
        return $this->client->getHttpClient()->get($this->client->getBaseUrl() . $path, $this->options)->getBody()->getContents();
    }

    /**
     * @param $path
     * @param array $params
     * @return string
     */
    protected function post($path, array $params = []){
        $this->options['query'] = $params;
        return $this->client->getHttpClient()->post($this->client->getBaseUrl() . $path, $this->options)->getBody()->getContents();
    }

    /**
     * @param $path
     * @param array $params
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    protected function postAsync($path, array $params = []){
        $this->options['query'] = $params;
        return $this->client->getHttpClient()->postAsync($this->client->getBaseUrl() . $path, $this->options);
    }

    /**
     * @param $path
     * @param array $params
     * @return string
     */
    protected function put($path, array $params = []){
        $this->options['form_params'] = $params;
        return $this->client->getHttpClient()->put($this->client->getBaseUrl() . $path, $this->options)->getBody()->getContents();
    }

    /**
     * @param $path
     * @param array $params
     */
    protected function putAsync($path, array $params = []){
        $this->options['form_params'] = $params;
        $this->client->getHttpClient()->putAsync($this->client->getBaseUrl() . $path, $this->options);
    }

    /**
     * @param $path
     * @param array $params
     * @return string
     */
    protected function delete($path, array $params = []){
        $this->options['query'] = $params;
        return $this->client->getHttpClient()->delete($this->client->getBaseUrl() . $path, $this->options)->getBody()->getContents();
    }

    /**
     * @param $path
     * @param array $params
     */
    protected function deleteAsync($path, array $params = []){
        $this->options['query'] = $params;
        $this->client->getHttpClient()->deleteAsync($this->client->getBaseUrl() . $path, $this->options);
    }

}