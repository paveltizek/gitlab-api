<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 9.7.17
 * Time: 11:44
 */

namespace PavelTizek\GitLab\Api;


use Nette\Utils\Json;

use PavelTizek\GitLab\Factory\IssueFactory;
use PavelTizek\GitLab\Model\Issue;
use PavelTizek\GitLab\Model\IssueComment;

class IssueApi extends AbstractApi
{


    /**
     * @return Issue[]
     */
    public function getAll(): array
    {
        $query = [];

        $jsonIssues = Json::decode($this->get('/issues', $query), Json::FORCE_ARRAY);

        $issues = [];

        foreach ($jsonIssues as $jsonIssue) {


            $milestone = null;
            if (isset($jsonIssue['milestone'])) {

                $milestone = $this->milestoneFactory->create($jsonIssue['milestone']);
            }


            $author = null;
            if (isset($jsonIssue['author'])) {

                $author = $this->authorFactory->create($jsonIssue['author']);
            }


            $issues[] = $this->issueFactory->create($jsonIssue, $milestone, $author);
        }

        return $issues;

    }

	/**
	 * @param int $projectId
	 * @param string $title
	 * @param string $description
	 * @param array $labels
	 * @param \DateTimeInterface|null $dueDate
	 * @param int $weight
	 * @param array $userIds
	 * @return Issue
	 */
    public function createIssue(
    	int $projectId,
	    string $title,
	    string $description,
	    array $labels,
		\DateTimeInterface $dueDate = null,
		int $weight = 0,
	array $userIds = []
    ): Issue
    {

        $labelString = '';

        foreach ($labels as $label) {
            $labelString .= $label . ',';
        }

        $labelString = rtrim($labelString, ',');

        $data = [
            'title' => $title,
            'description' => $description,
            'labels' => $labelString,
	        'weight' => $weight
        ];

        if($dueDate){
        	$data['due_date'] = $dueDate->format('Y-m-d');
        }

	    if(!empty($userIds)){
		    $data['assignee_ids'] = $userIds;
	    }

        $jsonIssue = Json::decode($this->post('/projects/' . $projectId . '/issues', $data), Json::FORCE_ARRAY);

        $author = $this->authorFactory->create($jsonIssue['author']);

        $milestone = null;
        if (isset($jsonIssue['milestone'])) {
            $milestone = $this->milestoneFactory->create($jsonIssue['milestone']);
        }
        $issue = $this->issueFactory->create($jsonIssue, $milestone, $author, []);


        return $issue;
    }

    /**
     * @param $projectId
     * @param $issueId
     * @return Issue
     */
    public function getById($projectId, $issueId): Issue
    {
        $jsonIssue = Json::decode($this->get('/projects/' . $projectId . '/issues/' . $issueId), Json::FORCE_ARRAY);

        $jsonComments = Json::decode($this->get('/projects/' . $projectId . '/issues/' . $issueId . '/notes'), Json::FORCE_ARRAY);


        $comments = [];
        foreach ($jsonComments as $jsonComment) {


            $issueCommentAuthor = $this->noteAuthorFactory->create($jsonComment['author']);
            $comments[] = $this->issueCommentFactory->create($jsonComment, $issueCommentAuthor);
        }


        $author = null;
        if ($jsonIssue['author']){

            $author = $this->authorFactory->create($jsonIssue['author']);
        }
        $milestone = null;
        if (isset($jsonIssue['milestone'])) {

            $milestone = $this->milestoneFactory->create($jsonIssue['milestone']);
        }
        $issue = $this->issueFactory->create($jsonIssue, $milestone, $author, $comments);


        return $issue;
    }

    /**
     * @param $projectId
     * @param $issueId
     * @param $body
     * @param null $createdAt
     * @return IssueComment
     */
    public function addComment($projectId, $issueId, $body, $createdAt = null):IssueComment
    {

        $args = [
            'body' => $body,
            'created_at' => $createdAt,
        ];

        $jsonComment = Json::decode($this->post('/projects/' . $projectId . '/issues/' . $issueId . '/notes', $args), Json::FORCE_ARRAY);
        $issueCommentAuthor = $this->noteAuthorFactory->create($jsonComment['author']);


        return $this->issueCommentFactory->create($jsonComment, $issueCommentAuthor);
    }

    /**
     * @param $projectId
     * @param $issueId
     * @param $commentId
     * @param $body
     * @return IssueComment
     */
    public function editComment($projectId, $issueId, $commentId, $body): IssueComment
    {

        $args = [
            'body' => $body,
        ];

        $jsonComment = Json::decode($this->put('/projects/' . $projectId . '/issues/' . $issueId . '/notes/' . $commentId, $args), Json::FORCE_ARRAY);
        $issueCommentAuthor = $this->noteAuthorFactory->create($jsonComment['author']);


        return $this->issueCommentFactory->create($jsonComment, $issueCommentAuthor);
    }

    /**
     * @param $projectId
     * @param array $labels
     * @return Issue[]
     */
    public function getByLabel($projectId, array $labels)
    {
        $labelString = '';

        foreach ($labels as $label) {
            $labelString .= $label . ',';
        }

        $labelString = rtrim($labelString, ',');
        $data = [
            'labels' => $labelString,
        ];


        $jsonIssues = Json::decode($this->get('/projects/' . $projectId . '/issues/', $data), Json::FORCE_ARRAY);

        $issues = [];

        foreach ($jsonIssues as $jsonIssue) {


            $milestone = null;
            if (isset($jsonIssue['milestone'])) {

                $milestone = $this->milestoneFactory->create($jsonIssue['milestone']);
            }


            $author = null;
            if (isset($jsonIssue['author'])) {

                $author = $this->authorFactory->create($jsonIssue['author']);
            }


            $issues[] = $this->issueFactory->create($jsonIssue, $milestone, $author);
        }

        return $issues;

    }


    /**
     * @param $projectId
     * @param bool $open
     * @return Issue[]
     */
    public function getByState($projectId, $open = true)
    {

        $data = [
            'state' => $open ? 'open' : 'closed',
        ];

        $jsonIssues = Json::decode($this->get('/projects/' . $projectId . '/issues/', $data), Json::FORCE_ARRAY);

        $issues = [];

        foreach ($jsonIssues as $jsonIssue) {

            $milestone = null;
            if (isset($jsonIssue['milestone'])) {

                $milestone = $this->milestoneFactory->create($jsonIssue['milestone']);
            }


            $author = null;
            if (isset($jsonIssue['author'])) {

                $author = $this->authorFactory->create($jsonIssue['author']);
            }

            $issues[] = $this->issueFactory->create($jsonIssue, $milestone, $author);
        }

        return $issues;


    }

    /**
     * @param $projectId
     * @param $issueId
     * @param $data
     * @return mixed
     */
    public function editIssue($projectId, $issueId, $data):Issue
    {
        $args = [
            'body' => $data,
        ];

        $jsonIssue = Json::decode($this->put('/projects/' . $projectId . '/issues/' . $issueId, $data), Json::FORCE_ARRAY);

        return $this->issueFactory->create($jsonIssue);
    }


}