<?php
/**
 * User: paveltizek
 * Date: 2.8.17
 * Time: 7:38
 */

namespace PavelTizek\GitLab\Api;


use Nette\Utils\Json;
use PavelTizek\GitLab\Model\BroadcastMessage;

class BroadcastMessageApi extends AbstractApi
{

    /**
     * @return BroadcastMessage[]
     */
    public function getAll(){
        $jsonBroadcastMessages = Json::decode($this->get('/broadcast_messages'), Json::FORCE_ARRAY);
        $broadcastMessages = [];

        foreach ($jsonBroadcastMessages as $jsonBroadcastMessage) {


            $broadcastMessages[] = $this->broadcastMessageFactory->create($jsonBroadcastMessage);
        }

        return $broadcastMessages;
    }
}