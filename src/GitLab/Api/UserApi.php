<?php
/**
 * Created by PhpStorm.
 * User: paveltizek
 * Date: 12.7.17
 * Time: 18:36
 */

namespace PavelTizek\GitLab\Api;


class UserApi extends AbstractApi
{

	/**
	 * @return Issue[]
	 */
	public function getAll(): array
	{
		$query = [];
		$jsonUsers = Json::decode($this->get('/users', $query), Json::FORCE_ARRAY);
		$users = [];

		foreach ($jsonUsers as $user) {
			$users[] = $this->userFactory->create($user);
		}

		return $users;

	}

}