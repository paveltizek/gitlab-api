<?php
/**
 * User: paveltizek
 * Date: 6.7.17
 * Time: 6:54
 */

namespace PavelTizek\GitLab\Api;




class ProjectApi extends AbstractApi
{
    /**
     * @return string
     */
    public function myProjects()
    {
        $query = ['owned' => 'true'];

        return $this->get('/projects', $query);

    }

}