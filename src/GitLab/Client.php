<?php
/**
 * User: paveltizek
 * Date: 6.7.17
 * Time: 6:40
 */

namespace PavelTizek\GitLab;



use PavelTizek\GitLab\Api\BroadcastMessageApi;
use PavelTizek\GitLab\Api\DeployKeyApi;
use PavelTizek\GitLab\Api\GroupApi;
use PavelTizek\GitLab\Api\IssueApi;
use PavelTizek\GitLab\Api\JobApi;
use PavelTizek\GitLab\Api\KeyApi;
use PavelTizek\GitLab\Api\MergeRequestApi;
use PavelTizek\GitLab\Api\MilestoneApi;
use PavelTizek\GitLab\Api\ProjectApi;
use PavelTizek\GitLab\Api\ProjectNamespaceApi;
use PavelTizek\GitLab\Api\RepositoryApi;
use PavelTizek\GitLab\Api\TagApi;
use PavelTizek\GitLab\Api\UserApi;
use PavelTizek\GitLab\Api\VersionApi;

class Client
{
    const PRIVATE_ACCESS_TOKEN = 'private_token';

    /** @var \GuzzleHttp\Client */
    private $httpClient;

    private $baseUrl;

    private $options;

    /**
     * Client constructor.
     * @param $baseUrl
     */
    public function __construct($baseUrl)
    {
        $this->baseUrl = $baseUrl . '/api/v4';
        $this->httpClient = new \GuzzleHttp\Client();
        $this->options['connect_timeout'] = 2;


    }

    public function authenticate($token, $method = self::PRIVATE_ACCESS_TOKEN){
        switch ($method){
            case self::PRIVATE_ACCESS_TOKEN:
                $this->options['headers'] = [
                    'PRIVATE-TOKEN' => $token
                ];
                break;
            default:
                $this->options['headers'] = [
                    'PRIVATE-TOKEN' => $token
                ];
        }

    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @param \GuzzleHttp\Client $httpClient
     */
    public function setHttpClient($httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }


    /**
     * @param $apiName
     * @return null|ProjectNamespaceApi|RepositoryApi|UserApi|VersionApi
     */
    public function api($apiName)
    {
        switch ($apiName){
            case 'projects':
                return new ProjectApi($this);
            case 'issues':
                return new IssueApi($this);
            case 'tags':
                return new TagApi($this);
            case 'deployKeys':
                return new DeployKeyApi($this);
            case 'groups':
                return new GroupApi($this);
            case 'jobs':
                return new JobApi($this);
            case 'keys':
                return new KeyApi($this);
            case 'mergeRequests':
                return new MergeRequestApi($this);
            case 'milestones':
                return new MilestoneApi($this);
            case 'projectNamespaces':
                return new ProjectNamespaceApi($this);
            case 'repositories':
                return new RepositoryApi($this);
            case 'users':
                return new UserApi($this);
            case 'version':
                return new VersionApi($this);
            default:
                return null;
        }
    }

    /**
     * @return ProjectApi
     */
    public function projects(){
        return new ProjectApi($this);
    }

    /**
     * @return IssueApi
     */
    public function issues(){
        return new IssueApi($this);
    }

    /**
     * @return TagApi
     */
    public function tags(){
        return new TagApi($this);
    }

    /**
     * @return DeployKeyApi
     */
    public function deployKeys(){
        return new DeployKeyApi($this);
    }

    /**
     * @return GroupApi
     */
    public function groups(){
        return new GroupApi($this);
    }

    /**
     * @return JobApi
     */
    public function jobs(){
        return new JobApi($this);
    }

    /**
     * @return KeyApi
     */
    public function keys(){
        return new KeyApi($this);
    }

    /**
     * @return MergeRequestApi
     */
    public function mergeRequests(){
        return new MergeRequestApi($this);
    }

    /**
     * @return MilestoneApi
     */
    public function milestones(){
        return new MilestoneApi($this);
    }

    /**
     * @return ProjectNamespaceApi
     */
    public function projectNamespaces(){
        return new ProjectNamespaceApi($this);
    }

    /**
     * @return RepositoryApi
     */
    public function repositories(){
        return new RepositoryApi($this);
    }

    /**
     * @return UserApi
     */
    public function users(){
        return new UserApi($this);
    }

    /**
     * @return VersionApi
     */
    public function version(){
        return new VersionApi($this);
    }

    /**
     * @return BroadcastMessageApi
     */
    public function broadcastMessages(){
        return new BroadcastMessageApi($this);
    }




}